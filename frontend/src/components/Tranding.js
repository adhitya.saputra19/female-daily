import React, { Component } from "react";
import { Rate } from "antd";
import Slider from "react-slick";

import "./Tranding.scss";

const product1 = require("../assets/product1.PNG");
const product25 = require("../assets/product25.PNG");
const product3 = require("../assets/product3.PNG");
const product4 = require("../assets/product4.PNG");

class Tranding extends Component {
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return (
      <div style={{ width: "100%", marginTop: -25 }}>
        <Slider {...settings}>
          <div>
            <div className="main-container-tranding">
              <div className="card-container-tranding" style={{ padding: 20 }}>
                <div className="editor-card-tranding">
                  <div className="card-content-trandig">
                    <img className="product-image-tranding" src={product1} />
                    <div className="rating-container-tranding">
                      <p className="text-rating-tranding">4.9</p>
                      <Rate
                        disabled
                        value={4.9}
                        style={{ color: "#DB284E", margin: 5, fontSize: 7 }}
                      />
                      <p className="text-sum-rating-tranding">(7)</p>
                    </div>
                    <p className="txt-product-name-tranding">SKINCEUTICALS</p>
                    <p className="txt-product-desc-tranding">C E Ferulic</p>
                  </div>
                </div>
                <div className="editor-card-tranding">
                  <div className="card-content-trandig">
                    <img className="product-image-tranding" src={product25} />
                    <div className="rating-container-tranding">
                      <p className="text-rating-tranding">4.9</p>
                      <Rate
                        disabled
                        value={4.9}
                        style={{ color: "#DB284E", margin: 5, fontSize: 7 }}
                      />
                      <p className="text-sum-rating-tranding">(7)</p>
                    </div>
                    <p className="txt-product-name-tranding">JUICE BEAUTY</p>
                    <p className="txt-product-desc-tranding">
                      Phyto-Pigments Flawless Serum
                    </p>
                    <p className="txt-product-subdesc-tranding">Rosy Beige</p>
                  </div>
                </div>
                <div className="editor-card-tranding">
                  <div className="card-content-trandig">
                    <img className="product-image-tranding" src={product3} />
                    <div className="rating-container-tranding">
                      <p className="text-rating-tranding">4.9</p>
                      <Rate
                        disabled
                        value={4.9}
                        style={{ color: "#DB284E", margin: 5, fontSize: 7 }}
                      />
                      <p className="text-sum-rating-tranding">(7)</p>
                    </div>
                    <p className="txt-product-name-tranding">JUICE BEAUTY</p>
                    <p className="txt-product-desc-tranding">
                      Pure pressed Blush
                    </p>
                    <p className="txt-product-subdesc-tranding">Neutral rose</p>
                  </div>
                </div>
                <div className="editor-card-tranding">
                  <div className="card-content-trandig">
                    <img className="product-image-tranding" src={product4} />
                    <div className="rating-container-tranding">
                      <p className="text-rating-tranding">4.9</p>
                      <Rate
                        disabled
                        value={4.9}
                        style={{ color: "#DB284E", margin: 5, fontSize: 7 }}
                      />
                      <p className="text-sum-rating-tranding">(7)</p>
                    </div>
                    <p className="txt-product-name-tranding">
                      VAL BY VALERIE THOMAS
                    </p>
                    <p className="txt-product-desc-tranding">
                      Pure pressed Blush
                    </p>
                    <p className="txt-product-subdesc-tranding">Neutral rose</p>
                  </div>
                </div>
                <div className="editor-card-tranding">
                  <div className="card-content-trandig">
                    <img className="product-image-tranding" src={product1} />
                    <div className="rating-container-tranding">
                      <p className="text-rating-tranding">4.9</p>
                      <Rate
                        disabled
                        value={4.9}
                        style={{ color: "#DB284E", margin: 5, fontSize: 7 }}
                      />
                      <p className="text-sum-rating-tranding">(7)</p>
                    </div>
                    <p className="txt-product-name-tranding">SKINCEUTICALS</p>
                    <p className="txt-product-desc-tranding">C E Ferulic</p>
                  </div>
                </div>
                <div className="editor-card-tranding">
                  <div className="card-content-trandig">
                    <img className="product-image-tranding" src={product25} />
                    <div className="rating-container-tranding">
                      <p className="text-rating-tranding">4.9</p>
                      <Rate
                        disabled
                        value={4.9}
                        style={{ color: "#DB284E", margin: 5, fontSize: 7 }}
                      />
                      <p className="text-sum-rating-tranding">(7)</p>
                    </div>
                    <p className="txt-product-name-tranding">JUICE BEAUTY</p>
                    <p className="txt-product-desc-tranding">
                      Phyto-Pigments Flawless Serum
                    </p>
                    <p className="txt-product-subdesc-tranding">Rosy Beige</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <h3>page 2</h3>
          </div>
          <div>
            <h3>page 3</h3>
          </div>
        </Slider>
      </div>
    );
  }
}

export default Tranding;
