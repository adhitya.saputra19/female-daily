import React, { Component } from "react";

import "./Video.scss";

class Videos extends Component {
  render() {
    return (
      <div className="main-container-video">
        <div className="card-video-container">
          <div>
            <iframe
              width="676"
              height="366"
              src="https://www.youtube.com/embed/6LUdS0VFOJc"
              frameborder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
          </div>
          <div>
            <div>
              <iframe
                width="300"
                height="180"
                src="https://www.youtube.com/embed/2L_torpPFps"
                frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </div>
            <div>
              <iframe
                width="300"
                height="180"
                src="https://www.youtube.com/embed/FvbNZd-IxK4"
                frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Videos;
