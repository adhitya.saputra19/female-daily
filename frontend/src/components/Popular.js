import React, { Component } from "react";
import { Col } from "antd";
import {
  UnorderedListOutlined,
  UserOutlined,
  CommentOutlined
} from "@ant-design/icons";

import "./Popular.scss";

const foto1 = require("../assets/foto1.PNG");
const foto2 = require("../assets/foto2.PNG");
const foto3 = require("../assets/foto3.PNG");
const foto4 = require("../assets/foto4.PNG");

class Popular extends Component {
  render() {
    return (
      <div className="main-container-popular">
        <div className="card-popular-container" style={{ padding: 20 }}>
          <div className="popular-card">
            <div className="card-content">
              <img className="popular-image" src={foto1} />
              <p align="center" className="txt-popular-name">
                Embrace the Curl
              </p>
              <div align="center" className="action-container">
                <Col style={{ cursor: "pointer" }}>
                  <UserOutlined />
                </Col>
                <Col style={{ cursor: "pointer" }}>
                  <UnorderedListOutlined />
                </Col>
                <Col style={{ cursor: "pointer" }}>
                  <CommentOutlined />
                </Col>
              </div>
              <p align="center" className="txt-popular-desc">
                May our curls pop and never stop!{" "}
              </p>
            </div>
          </div>
          <div className="popular-card">
            <div className="card-content">
              <img className="popular-image" src={foto2} />
              <p align="center" className="txt-popular-name">
                Embrace the Curl
              </p>
              <div align="center" className="action-container">
                <Col style={{ cursor: "pointer" }}>
                  <UserOutlined />
                </Col>
                <Col style={{ cursor: "pointer" }}>
                  <UnorderedListOutlined />
                </Col>
                <Col style={{ cursor: "pointer" }}>
                  <CommentOutlined />
                </Col>
              </div>
              <p align="center" className="txt-popular-desc">
                May our curls pop and never stop!{" "}
              </p>
            </div>
          </div>
          <div className="popular-card">
            <div className="card-content">
              <img className="popular-image" src={foto3} />
              <p align="center" className="txt-popular-name">
                Embrace the Curl
              </p>
              <div align="center" className="action-container">
                <Col style={{ cursor: "pointer" }}>
                  <UserOutlined />
                </Col>
                <Col style={{ cursor: "pointer" }}>
                  <UnorderedListOutlined />
                </Col>
                <Col style={{ cursor: "pointer" }}>
                  <CommentOutlined />
                </Col>
              </div>
              <p align="center" className="txt-popular-desc">
                May our curls pop and never stop!{" "}
              </p>
            </div>
          </div>
          <div className="popular-card">
            <div className="card-content">
              <img className="popular-image" src={foto4} />
              <p align="center" className="txt-popular-name">
                Embrace the Curl
              </p>
              <div align="center" className="action-container">
                <Col style={{ cursor: "pointer" }}>
                  <UserOutlined />
                </Col>
                <Col style={{ cursor: "pointer" }}>
                  <UnorderedListOutlined />
                </Col>
                <Col style={{ cursor: "pointer" }}>
                  <CommentOutlined />
                </Col>
              </div>
              <p align="center" className="txt-popular-desc">
                May our curls pop and never stop!{" "}
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Popular;
