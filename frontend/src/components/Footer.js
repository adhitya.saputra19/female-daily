import React, { Component } from "react";
import { Row, Col, Divider } from "antd";

const facebook = require("../assets/facebook.PNG");
const ig = require("../assets/ig.PNG");
const twetter = require("../assets/twetter.PNG");
const yt = require("../assets/yt.PNG");
const playstore = require("../assets/playstore.PNG");
const Appstrore = require("../assets/Appstrore.PNG");
const LOGO = require("../assets/Logo.PNG");

class Footer extends Component {
  render() {
    return (
      <div>
        <Row>
          <Col>
            <ul>
              <ul style={{ cursor: "pointer" }}>About us</ul>
              <ul style={{ cursor: "pointer" }}>Feedback</ul>
              <ul style={{ cursor: "pointer" }}>Contact</ul>
            </ul>
          </Col>
          <Col>
            <ul>
              <ul style={{ cursor: "pointer" }}>Term & Conditions</ul>
              <ul style={{ cursor: "pointer" }}>Privacy Policy</ul>
              <ul style={{ cursor: "pointer" }}>Help</ul>
            </ul>
          </Col>
          <Col>
            <ul>
              <ul style={{ cursor: "pointer" }}>Awards</ul>
              <ul style={{ cursor: "pointer" }}>Newsletter</ul>
            </ul>
          </Col>
          <Col>
            <ul>
              <ul>Download Our Mobile App</ul>
              <ul>
                <img style={{ cursor: "pointer" }} src={Appstrore}></img>
                <img style={{ cursor: "pointer" }} src={playstore}></img>
              </ul>
            </ul>
          </Col>
        </Row>
        <Row>
          <Col>
            <img src={LOGO} style={{ marginBottom: 20 }}></img>
            <p
              style={{
                fontSize: 14,
                fontWeight: "bold",
                display: "flex",
                color: "#a0a0a0",
                marginTop: "-20px"
              }}
            >
              Copyright &copy; 2015 - 2017 Female Daily Network - All rights
              reserved
            </p>
          </Col>
          <Col>
            <Row style={{ marginTop: 10 }}>
              <img style={{ cursor: "pointer" }} src={facebook}></img>
              <img style={{ cursor: "pointer" }} src={twetter}></img>
              <img style={{ cursor: "pointer" }} src={ig}></img>
              <img style={{ cursor: "pointer" }} src={yt}></img>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Footer;
