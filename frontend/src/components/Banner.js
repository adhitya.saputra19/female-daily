import React, { Component } from "react";
import { Rate, Col, Row } from "antd";
import { inject, observer, toJS } from "mobx-react";
import * as mobx from "mobx";

import "./Banner.scss";
const gambar = require("../assets/banner.PNG");

class Banner extends Component {
  componentDidMount = () => {
    this.props.editorStore.editor();
  };
  render() {
    const { dataEditor, dataReview, dataArticles } = this.props.editorStore;

    return (
      <div className="banner-container">
        <div style={{ cursor: "pointer" }}>
          <Row>
            <Col md={5}>
              <div>
                <img
                  style={{ margin: "34px 0px 0px -23px" }}
                  className="gamabar"
                  src={gambar}
                />
              </div>
            </Col>
            <Col md={7}>
              <div className="con-all-ben">
                <div className="con-txt-ben">
                  <p
                    style={{ fontSize: 20, fontWeight: "bold", color: "black" }}
                  >
                    Looking for product that are just simply perfect for you?
                  </p>
                </div>
                <div className="con-txt-ben2">
                  <p style={{ fontSize: 15, color: "black" }}>
                    Here are some product that we belive match your skin, hair,
                    and body! Have we mentioned that solve your concerns too?
                  </p>
                </div>
                <div className="con-txt-ben3">
                  <button className="button">See My Matches </button>
                </div>
              </div>
            </Col>
            <Col md={12}>
              <div className="banner-card-container" style={{ padding: 20 }}>
                {dataReview.map((item, index) => (
                  <div className="card-banner" key={index}>
                    <div>
                      <img className="product-image" src={item.product.image} />
                      <div className="rating-container-bb">
                        <p className="text-rating-bb">{item.star}</p>
                        <Rate
                          disabled
                          value={item.star}
                          style={{ color: "#DB284E", margin: 5, fontSize: 8 }}
                        />
                        <p className="text-sum-rating-bb">(7)</p>
                      </div>
                      <p className="txt-bb-name">{item.product.name}</p>
                      <p className="txt-bb-desc">{item.product.desc}</p>
                      <p className="txt-bb">Neutral Rose</p>
                    </div>
                  </div>
                ))}
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

Banner = inject("editorStore")(observer(Banner));
export default Banner;
