import React, { Component } from "react";
import { Rate } from "antd";
import { inject, observer, toJS } from "mobx-react";
import * as mobx from "mobx";

import "./Editor.scss";

const Foto = require("../assets/foto1.PNG");

class Editor extends Component {
  componentDidMount = () => {
    this.props.editorStore.editor();
  };
  render() {
    const { dataEditor } = this.props.editorStore;

    return (
      <div className="main-container">
        <div className="card-container" style={{ padding: 20 }}>
          {dataEditor.map((item, index) => (
            <div className="editor-card" key={index}>
              <div className="card-header">
                <img className="header-image" src={Foto} />
                <div className="text-container">
                  <p className="text-name">{item.editor}</p>
                  <p className="text-job">{item.role}</p>
                </div>
              </div>
              <div className="card-content" style={{ cursor: "pointer" }}>
                <img className="product-image" src={item.product.image} />
                <div className="rating-container">
                  <p className="text-rating">{item.product.rating}</p>
                  <Rate
                    disabled
                    value={item.product.rating}
                    style={{ color: "#DB284E", margin: 5, fontSize: 8 }}
                  />
                  <p className="text-sum-rating">(7)</p>
                </div>
                <p className="txt-product-name">{item.product.name}</p>
                <p className="txt-product-desc">{item.product.description}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

Editor = inject("editorStore")(observer(Editor));
export default Editor;
