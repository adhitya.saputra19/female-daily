import React, { Component } from "react";
import { Rate } from "antd";
import { inject, observer, toJS } from "mobx-react";
import * as mobx from "mobx";

import "./Articles.scss";

class Articles extends Component {
  componentDidMount = () => {
    this.props.editorStore.editor();
  };
  render() {
    const { dataEditor, dataReview, dataArticles } = this.props.editorStore;

    return (
      <div className="main-con">
        <div className="card-articles" style={{ padding: 0 }}>
          {dataArticles.map((item, index) => (
            <div className="cont-card" key={index}>
              <img className="Articles-image" src={item.image} />
              <p className="txt-titles">{item.title}</p>
              <p className="txt-author">
                {item.author} | {item.published_at}
              </p>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

Articles = inject("editorStore")(observer(Articles));
export default Articles;
