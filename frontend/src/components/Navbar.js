import React, { Component } from "react";
import { Input, Button, Row, Col, Drawer } from "antd";
import Icon from "@ant-design/icons";
import { MenuOutlined, UserOutlined, SearchOutlined } from "@ant-design/icons";
import "./Navbar.scss";

const LOGO = require("../assets/Logo.PNG");

class Navbar extends Component {
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  render() {
    return (
      <nav className="menuBar">
        <Row>
          <Col md={1}>
            <Button
              onClick={this.showDrawer}
              style={{
                borderStyle: "none",
                marginTop: -4,
                height: "100%"
              }}
            >
              <Icon component={MenuOutlined} />
            </Button>
          </Col>
          <Col md={3}>
            <div style={{ paddingTop: 14, paddingLeft: 5, width: "100%" }}>
              <img src={LOGO}></img>
            </div>
          </Col>
          <Col md={16}>
            <div style={{ padding: 15 }}>
              <Input
                style={{ width: "100%" }}
                placeholder="Search product, articles, topics, brands, etc"
                prefix={<SearchOutlined />}
              />
            </div>
          </Col>
          <Col md={4}>
            <button
              className="login"
              style={{ background: "#d91c47", height: "100%", width: "100%" }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              >
                <div style={{ marginRight: 5, color: "white" }}>
                  <UserOutlined />
                </div>
                <p
                  style={{
                    margin: 0,
                    fontWeight: "bold",
                    fontSize: 13,
                    color: "white"
                  }}
                >
                  Login / Sign up
                </p>
              </div>
            </button>
          </Col>
          <Drawer
            title={<img src={LOGO}></img>}
            closable={true}
            placement="left"
            closable={false}
            onClose={this.onClose}
            visible={this.state.visible}
          >
            <p style={{ cursor: "pointer" }}>Menu 1</p>
            <p style={{ cursor: "pointer" }}>Menu 2</p>
            <p style={{ cursor: "pointer" }}>Menu 3</p>
          </Drawer>
        </Row>
      </nav>
    );
  }
}

export default Navbar;
