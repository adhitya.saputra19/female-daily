import React, { Component } from "react";
import { Row, Col, Divider } from "antd";

const brand1 = require("../assets/brand1.PNG");
const brand2 = require("../assets/brand2.PNG");
const brand3 = require("../assets/brand3.PNG");
const brand4 = require("../assets/brand4.PNG");
const brand5 = require("../assets/brand5.PNG");
const brand6 = require("../assets/brand6.PNG");

class TopBrands extends Component {
  render() {
    return (
      <div>
        <Row style={{ marginBottom: 20 }}>
          <Col>
            <img src={brand1} />
          </Col>
          <Col>
            <img src={brand2} />
          </Col>
          <Col>
            <img src={brand3} />
          </Col>
          <Col>
            <img src={brand4} />
          </Col>
          <Col>
            <img src={brand5} />
          </Col>
          <Col>
            <img src={brand6} />
          </Col>
        </Row>
        <p style={{ fontSize: 18, fontWeight: "bold", display: "flex" }}>
          Female Daily - Find everything you want to know about beauty on Female
          Daily
        </p>
        <p
          style={{
            fontSize: 14,
            fontWeight: "bold",
            display: "flex",
            marginTop: "-10px"
          }}
        >
          Product Reviews, Tips & Tricks, Expert and Consumer Opinions, Beauty
          Tutorials, Discussions, Beauty Workshops, anything!
        </p>
        <p
          style={{
            fontSize: 14,
            fontWeight: "bold",
            display: "flex",
            marginTop: "-20px"
          }}
        >
          We are here to be your friendly solution to all things beauty, inside
          and out!
        </p>
        <Divider style={{ marginTop: 50 }} />
      </div>
    );
  }
}

export default TopBrands;
