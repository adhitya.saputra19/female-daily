import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Provider } from "mobx-react";
import editorStore from "./store/editor";
import * as serviceWorker from "./serviceWorker";

const stores = {
  editorStore
};

ReactDOM.render(
  <Provider {...stores}>
    <App />
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
