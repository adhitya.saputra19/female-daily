import { observable, action, runInAction, decorate, toJS } from "mobx";

class EditorStore {
  dataEditor = [];
  dataArticles = [];
  dataReview = [];

  editor() {
    fetch("https://virtserver.swaggerhub.com/hqms/FDN-WP/0.1/wp", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(res => {
        runInAction(() => {
          this.dataEditor = res["editor's choice"];
          this.dataArticles = res["latest articles"];
          this.dataReview = res["latest review"];
        });
        console.log("dataArticles: ", toJS(this.dataArticles));
      })
      .catch(err => console.log(err));
  }
}

decorate(EditorStore, {
  dataEditor: observable,
  editor: action
});

export default new EditorStore();
