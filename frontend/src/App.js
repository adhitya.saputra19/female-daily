import React, { Component } from "react";
import { Card, Menu, Typography, Col, Row, Rate, Divider } from "antd";
import "./App.scss";
import "antd/dist/antd.css";

import Navbar from "./components/Navbar";
import Editor from "./components/Editor";
import Banner from "./components/Banner";
import Articles from "./components/Articles";
import Review from "./components/Review";
import Popular from "./components/Popular";
import Videos from "./components/Video";
import Tranding from "./components/Tranding";
import TopBrands from "./components/TopBrands";
import Footer from "./components/Footer";

const { Title } = Typography;

class App extends Component {
  render() {
    return (
      <>
        <div className="header">
          <Navbar />
        </div>
        <div align="center">
          <Menu mode="horizontal">
            <Menu.Item key="1">SKINCARE</Menu.Item>
            <Menu.Item key="2">MAKE UP</Menu.Item>
            <Menu.Item key="3">BODY</Menu.Item>
            <Menu.Item key="4">HAIR</Menu.Item>
            <Menu.Item key="5">FRAGRANCE</Menu.Item>
            <Menu.Item key="6">NAILS</Menu.Item>
            <Menu.Item key="7">TOOLS</Menu.Item>
            <Menu.Item key="8">BRANDS</Menu.Item>
          </Menu>
        </div>
        <div style={{ display: "flex", justifyContent: "center", padding: 20 }}>
          <div
            className="topframe"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <p
              style={{
                margin: 8,
                color: "#5a5a5a",
                fontSize: 18,
                fontWeight: "bold"
              }}
              level={3}
            >
              Top Frame
            </p>
          </div>
        </div>

        <div style={{ display: "flex", justifyContent: "center", padding: 20 }}>
          <div
            className="billboard"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <p
              style={{
                margin: 100,
                color: "#5a5a5a",
                fontSize: 18,
                fontWeight: "bold"
              }}
              level={3}
            >
              Billboard
            </p>
          </div>
        </div>
        {/* editor choise */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: 20
          }}
        >
          <div className="card-editor">
            <p style={{ fontSize: 18, fontWeight: "bold", display: "flex" }}>
              Editor's Choice
            </p>
            <p
              style={{
                fontSize: 14,
                fontWeight: "bold",
                display: "flex",
                color: "#a0a0a0",
                marginTop: "-20px"
              }}
            >
              Curated with love
            </p>
            <Editor />
          </div>
        </div>
        {/* banner */}
        <div
          style={{
            display: "flex",
            justifyContent: "center"
          }}
        >
          <Card
            className="banner"
            style={{ background: "#fddae0", margin: 30 }}
            bordered={false}
          >
            <Banner />
          </Card>
        </div>
        {/* internal campaign */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: 20
          }}
        >
          <div
            className="billboard"
            style={{
              display: "flex",
              justifyContent: "center"
            }}
          >
            <p
              style={{
                margin: 100,
                color: "#5a5a5a",
                fontSize: 18,
                fontWeight: "bold"
              }}
              level={3}
            >
              Internal campaign only
            </p>
          </div>
        </div>
        {/* latest article */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: 20
          }}
        >
          <div className="card-editor">
            <p style={{ fontSize: 18, fontWeight: "bold", display: "flex" }}>
              Latest Articles
            </p>
            <Row>
              <Col span={8} align="left">
                <p
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    display: "flex",
                    color: "#a0a0a0",
                    marginTop: "-20px"
                  }}
                >
                  So you can make better purchase decision
                </p>
              </Col>
              <Col span={8} offset={8} align="right">
                <p className="seemore">See more ></p>
              </Col>
            </Row>
            <Articles />
          </div>
        </div>
        {/* latest review */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: 20
          }}
        >
          <Col md={18}>
            <div className="card-review-con">
              <p style={{ fontSize: 18, fontWeight: "bold", display: "flex" }}>
                Latest Reviews
              </p>
              <Row>
                <Col span={8} align="left">
                  <p
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      display: "flex",
                      color: "#a0a0a0",
                      marginTop: "-20px"
                    }}
                  >
                    So you can make better purchase decision
                  </p>
                </Col>
                <Col span={8} offset={8} align="right">
                  <p
                    align="left"
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      color: "#DC2F53",
                      marginTop: "-20px",
                      cursor: "pointer"
                    }}
                  >
                    See more >
                  </p>
                </Col>
              </Row>
              <Review />
            </div>
          </Col>
          <Col md={6}>
            <div className="MR">
              <p
                style={{
                  color: "#5a5a5a",
                  fontSize: 18,
                  fontWeight: "bold",
                  display: "flex",
                  justifyContent: "center",
                  margin: 100
                }}
                level={3}
              >
                MR 2
              </p>
            </div>
          </Col>
        </div>
        {/* popular group */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: 20,
            marginTop: -40
          }}
        >
          <div className="card-editor">
            <p style={{ fontSize: 18, fontWeight: "bold", display: "flex" }}>
              Popular Groups
            </p>
            <Row>
              <Col span={8} align="left">
                <p
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    display: "flex",
                    color: "#a0a0a0",
                    marginTop: "-20px"
                  }}
                >
                  Where the beauty TALK are
                </p>
              </Col>
              <Col span={8} offset={8} align="right">
                <p className="seemore">See more ></p>
              </Col>
            </Row>
            <Popular />
          </div>
        </div>
        {/* latest video */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: 20,
            marginTop: -40
          }}
        >
          <div className="card-editor">
            <p style={{ fontSize: 18, fontWeight: "bold", display: "flex" }}>
              Latest Video
            </p>
            <Row>
              <Col span={8} align="left">
                <p
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    display: "flex",
                    color: "#a0a0a0",
                    marginTop: "-20px"
                  }}
                >
                  Watch and learn, ladies
                </p>
              </Col>
              <Col span={8} offset={8} align="right">
                <p className="seemore">See more ></p>
              </Col>
            </Row>
            <Videos />
          </div>
        </div>
        {/* Tranding */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: " 0px 20px 0px"
          }}
        >
          <div className="card-tranding-con">
            <p style={{ fontSize: 18, fontWeight: "bold", display: "flex" }}>
              Tranding this week
            </p>
            <p
              style={{
                fontSize: 14,
                fontWeight: "bold",
                display: "flex",
                color: "#a0a0a0",
                marginTop: "-20px"
              }}
            >
              See our weekly most reviewed products
            </p>
            <Tranding />
          </div>
        </div>
        {/* Top Brand */}
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: 20,
            marginTop: -40
          }}
        >
          <div className="card-editor">
            <p style={{ fontSize: 18, fontWeight: "bold", display: "flex" }}>
              Top Brands
            </p>
            <Row>
              <Col span={8} align="left">
                <p
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    display: "flex",
                    color: "#a0a0a0",
                    marginTop: "-20px"
                  }}
                >
                  We all know and love
                </p>
              </Col>
              <Col span={8} offset={8} align="right">
                <p className="seemore">See more ></p>
              </Col>
            </Row>
            <TopBrands />
            <Footer />
          </div>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: 20
          }}
        >
          <div
            className="bottomframe"
            style={{
              display: "flex",
              justifyContent: "center"
            }}
          >
            <p
              style={{
                margin: 8,
                color: "#5a5a5a",
                fontSize: 18,
                fontWeight: "bold"
              }}
              level={3}
            >
              Bottom Frame
            </p>
          </div>
        </div>
      </>
    );
  }
}

export default App;
